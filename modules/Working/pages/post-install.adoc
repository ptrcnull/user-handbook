= Post Installation Recommendations

Now that your installation of Alpine Linux is up and running, you can start working with it.
The following sections will provide a list of general recommendations to ease your interactive experience - they are all optional.
The remaining sections will describe how to use (on a user level) various Alpine-native solutions, such as the package manager, firewall, and so on.

== Creating a Normal User
Now that you are up and running, you will want a normal, non-root user to perform most daily tasks with.
You can either use the built-in busybox utility `adduser`, or the utility available in the `shadow` package named `useradd`.

Here are examples for creating a user (named "john" and "jane" respectively) using the utilities `adduser` and `useradd` respectively.

[source, sh]
----
adduser -h /home/john -s /bin/ash john <1>
----
<1> Both the `-h /home/john` and `-s /bin/ash` sections may be optional. However, it is recommended to specify both, as the defaults may not be desirable.

[source, sh]
----
useradd -m -U -s /bin/ash jane <1>
----
<1> The options are, as in the previous example, optional. However, they are still highly recommended, as shown.

Once your user has been created, if the utility you used has not asked you to set a password, you should do so now, using `passwd foo`, where "foo" is the username in question.

== Granting Your User Administrative Access
Sometimes, you'll want to do something that *does* require administrative powers.
While you may switch to a different tty and log in as root, this is often inconvenient.
You may gain root privileges ad-hoc using either the built-in busybox utility `su`, or the common external utility `doas`, available in the package named the same way.
`doas`, unlike `su`, will requires edditional configuration.
The difference between `doas` and `su` comes down to which side the permissions come from - `su` allows you to temporarily log-in as another user (and thus requires that you enter the password of the user you wish to log in as), while `doas` allows you to perform commands (including login shells) as the target user, assuming the configuration gives you that right (meaning that your password is the one used for authentication).
Here are examples on how to use `su`, and how to configure *and* use `doas` (in a shortened form) respectively:

[source, sh]
----
su -l root <1>
su - <2>
----
<1> `-l` means to run a login shell.
<2> A mere `-` implies `-l`, and if no user is mentioned, root is implied - this is equivalent to the example in <1>

[source, sh]
----
apk add doas <1>
echo 'permit :wheel' > /etc/doas.d/doas.conf <2>
adduser joe -G wheel <3>
su -l joe <4>
doas command with arguments <5>
----
<1> doas is not installed by default.
<2> By default, `doas` only provides permissions to root. This translates as "people in the group `wheel` are allowed to perform any command, as any user, and any group."
<3> The wheel group mentioned above is the common "administrator" group, and since we're using it, we need to add our user to said group.
<4> You may need to log out and log back in for the group listing to update.
<5> This will run "command with arguments" as the default `doas` user - root.

// COMBAK: alpine-desktop, alpine-conf (setup-xorg-base)
== Getting a Graphical Environment
Most people will often want something more than just a raw tty.
This section describes how to get a supported graphical interface, as well as some additional notes (for example, how to use a different type of GUI).

// TODO: link to acf
NOTE: You should *not* follow this if your system is intended to be used as a server, gateway, or similar device - those do not and should not require graphical interfaces. You can see the section acf if you absolutely require something more visual.

First, it is necessary to prepare the system - drivers are built into the linux kernel, but they are not accessible to the X Server - the windowing system provider.
On most hardware, you can do this quickly by running `setup-xorg-base`.
This will automatically detect what kind of gpu is installed, and install the appropriate xorg driver accelerator.

NOTE: If this does not work for you, or you encounter other issues, as per usual, feel free to ask for help in the official support channels.

// COMBAK: alpine-desktop contents
Once that is done, you can install the recommended and supported graphical setup by installing (and thus running) `apk add alpine-desktop`.
This will install several system-related utilities, as well as the `lxdm` Desktop Manager and `xfce4` Desktop Environment.
In case you want a different DE or DM, you should install those now.

Once this is done, you should enable your chosen Desktop Manager as a service.
For `lxdm` this looks like so: `rc-update add lxdm` and `rc-service lxdm start`.
The first one makes it start on boot, and the last one will start it up immediately.
